workflow jgi_meta {
    File input_file
    Float uniquekmer=1000
    String bbtools_container="bryce911/bbtools:38.44"
    String spades_container="bryce911/spades:3.13.0"
    String basic_container="bryce911/bbtools:38.44"
     #shooting for 30% occupancy based on: occupancy = (unique kmers)/((RAM bytes*0.7)*8/12)
    Int bbcms_mem_pred = ceil(( uniquekmer /0.138)/1073741824)
    call get_queue as bbcms_mem{
    	 input: request=bbcms_mem_pred, container=basic_container
    }
    call bbcms {
          input: infile=input_file, memfile=bbcms_mem.out, container=bbtools_container
    }
    call get_queue as spades_mem{
     	 input: request=120, container=basic_container 
    }
    call assy {
         input: infile1=bbcms.out1, infile2=bbcms.out2, memfile=spades_mem.out, container=spades_container
    }
    call get_queue as create_agp_mem{
     	 input: request=50, container=basic_container
    }
    call create_agp {
         input: scaffolds_in=assy.out, memfile=create_agp_mem.out, container=bbtools_container
    }
    call get_queue as bbmap_mem{
     input: request=create_agp.bbmap_mem_pred, container=basic_container 
    }
    call read_mapping_pairs {
     input: reads=input_file, ref=create_agp.outcontigs, memfile=bbmap_mem.out, container=bbtools_container
    }

}
#AWS_uswest2-optimal-ceq
task read_mapping_pairs{
    File reads
    File ref
    File memfile   
    String container
  
    Map [String,String] map = read_json(memfile)

    String filename_resources="resources.log"
    String filename_unsorted="pairedMapped.bam"
    String filename_outsam="pairedMapped.sam.gz"
    String filename_sorted="pairedMapped_sorted.bam"
    String filename_sorted_idx="pairedMapped_sorted.bam.bai"
    String filename_bamscript="to_bam.sh"
    String filename_cov="covstats.txt"
    String dollar="$"
    #runtime { backend: "Local"}
     runtime {
            docker: container
            backend: "genomics2-optimal-spot-ceq"
            memory: map["mem"]
	    cpu:  map["cpu"]
     }
    command{
    	echo $(curl --fail --max-time 10 --silent http://169.254.169.254/latest/meta-data/public-hostname)
        touch ${filename_resources};
	curl --fail --max-time 10 --silent https://bitbucket.org/berkeleylab/jgi-meta/get/master.tar.gz | tar --wildcards -zxvf - "*/bin/resources.bash" && ./*/bin/resources.bash > ${filename_resources} &
        sleep 30

        set -eo pipefail
	bbmap.sh ${map["java"]} threads=${map["cpu"]} nodisk=true interleaved=true ambiguous=random in=${reads} ref=${ref} out=${filename_unsorted} covstats=${filename_cov} bamscript=${filename_bamscript}
	samtools sort -m100M -@ ${map["cpu"]} ${filename_unsorted} -o ${filename_sorted}
        samtools index ${filename_sorted}
        reformat.sh ${map["java"]} in=${filename_unsorted} out=${filename_outsam} overwrite=true
  }
  output{
      File outbamfile = filename_sorted
      File outbamfileidx = filename_sorted_idx
      File outcovfile = filename_cov
      File outsamfile = filename_outsam
      File outresources = filename_resources
  }
}

task create_agp {
    File scaffolds_in
    File memfile       
    String container

    Map [String,String] map = read_json(memfile)
    
    String filename_resources="resources.log"
    String prefix="assembly"
    String filename_contigs="${prefix}.contigs.fasta"
    String filename_scaffolds="${prefix}.scaffolds.fasta"
    String filename_agp="${prefix}.agp"
    String filename_legend="${prefix}.scaffolds.legend"
#    runtime {backend: "Local"} 
     runtime {
            docker: container
            backend: "AWS_uswest2-optimal-ceq"
            memory: map["mem"]
	    cpu:  map["cpu"]
     }
    command{
	echo $(curl --fail --max-time 10 --silent http://169.254.169.254/latest/meta-data/public-hostname)
        touch ${filename_resources};
	curl --fail --max-time 10 --silent https://bitbucket.org/berkeleylab/jgi-meta/get/master.tar.gz | tar --wildcards -zxvf - "*/bin/resources.bash" && ./*/bin/resources.bash > ${filename_resources} &	
        sleep 30

	fungalrelease.sh ${map["java"]} in=${scaffolds_in} out=${filename_scaffolds} outc=${filename_contigs} agp=${filename_agp} legend=${filename_legend} mincontig=200 minscaf=200 sortscaffolds=t sortcontigs=t overwrite=t
  }
    output{
	File outcontigs = filename_contigs
	File outscaffolds = filename_scaffolds
	File outagp = filename_agp
    	File outlegend = filename_legend
    	Int bbmap_mem_pred = ceil((size(filename_contigs) * 7)/(1073741824) + 1.0) # 7 bytes per reference base + 400 byes/contig 
    	File outresources = filename_resources
    }
}

task assy {
     File infile1
     File infile2
     File memfile
     String container

     Map [String,String] map = read_json(memfile)
     #Int mem = map["mem_safe"]
     #String fail_me = if mem  < 115 then "/bin/true" else "/bin/false"

     String filename_resources="resources.log"
     String outprefix="spades3"
     String filename_outfile="${outprefix}/scaffolds.fasta"
     String filename_spadeslog ="${outprefix}/spades.log"
     String dollar="$"
#     runtime {backend: "Local"}
     runtime {
            docker: container
            backend: "AWS_uswest2-optimal-ceq"
            memory: map["mem"]
	    cpu:  map["cpu"]
     }
     command{
	echo $(curl --fail --max-time 10 --silent http://169.254.169.254/latest/meta-data/public-hostname)
        touch ${filename_resources};
	curl --fail --max-time 10 --silent https://bitbucket.org/berkeleylab/jgi-meta/get/master.tar.gz | tar --wildcards -zxvf - "*/bin/resources.bash" && ./*/bin/resources.bash > ${filename_resources} &		
        sleep 30
	
        set -eo pipefail
	echo ${map["request"]}
	echo ${map["mem_safe"]}
        spades.py -m 2000 -o ${outprefix} --only-assembler -k 33,55,77,99,127  --meta -t ${dollar}(grep "model name" /proc/cpuinfo | wc -l) -1 ${infile1} -2 ${infile2}
     }
     output {
            File out = filename_outfile
            File outlog = filename_spadeslog
            File outresources = filename_resources
     }
}

task bbcms {
     File infile
     File memfile
     String container

     Map [String,String] map = read_json(memfile)
     String filename_resources="resources.log"
     String filename_outfile="input.corr.fastq.gz"
     String filename_outfile1="input.corr.left.fastq.gz"
     String filename_outfile2="input.corr.right.fastq.gz"
     String filename_readlen="readlen.txt"
     String filename_outlog="stdout.log"
     String filename_errlog="stderr.log"
     String filename_kmerfile="unique31mer.txt"
     String filename_counts="counts.metadata.json"
     String dollar="$"
#     runtime { backend: "Local"} 
     runtime {
            docker: container
            backend: "AWS_uswest2-optimal-ceq"
            memory: map["mem"]
	    cpu:  map["cpu"]
     }
     command {
        echo $(curl --fail --max-time 10 --silent http://169.254.169.254/latest/meta-data/public-hostname)
        touch ${filename_resources};
	curl --fail --max-time 10 --silent https://bitbucket.org/berkeleylab/jgi-meta/get/master.tar.gz | tar --wildcards -zxvf - "*/bin/resources.bash" && ./*/bin/resources.bash > ${filename_resources} &		
        sleep 30

        set -eo pipefail
        bbcms.sh ${map["java"]}  metadatafile=${filename_counts} mincount=2 highcountfraction=0.6 in=${infile} out=${filename_outfile} > >(tee -a ${filename_outlog}) 2> >(tee -a ${filename_errlog} >&2) && grep Unique ${filename_errlog} | rev |  cut -f 1 | rev  > ${filename_kmerfile}
        reformat.sh ${map["java"]} in=${filename_outfile} out1=${filename_outfile1} out2=${filename_outfile2}
        readlength.sh ${map["java"]} in=${filename_outfile} out=${filename_readlen}
     }
     output {
            File out = filename_outfile
            File out1 = filename_outfile1
            File out2 = filename_outfile2
            File outreadlen = filename_readlen
            File stdout = filename_outlog
            File stderr = filename_errlog
            File outcounts = filename_counts
            File outkmer = filename_kmerfile
            Int predicted_mem = ceil((read_float(filename_kmerfile) * 0.00000002962 + 16.3) * 1.3 )
            File outresources = filename_resources
     }
}

task get_queue {
    String container
    String request
    Int increment = 0
    String outfile="mem.json"
#    runtime {backend: "Local"}
     runtime {
             docker: container
             backend: "AWS_uswest2-optimal-ceq"
             memory: "118 GiB"
             cpu: 16
    }
    command {
python <<CODE
import sys,json
request = int(${request})
#for r4 family
bins = [30, 60, 121, 243, 487]
cpus = [4, 8, 16, 32, 64]
val = [i for i in bins if request < i][0]
val = bins[bins.index(val)+${increment}] # adjust for attempt add the next bucket
cpu  = cpus[bins.index(val)]
out=dict()
out['request']=request
out['mem'] = float(val) - 1.0
out['mem_safe'] = int(out['mem'] * 0.95)
out['mem'] = str(int(out['mem'])) + " GiB"
out['cpu'] = str(cpu)
out['java'] = '-Xmx' + str(out['mem_safe']) + 'g'
with open("${outfile}","w") as f:f.write(json.dumps(out,indent=3))
CODE
    }
    output {
           File out = outfile
    }
}
