#!/usr/bin/env python

import sys, os
import subprocess
import shutil
'''
Get subset of test files
'''

HEAD_LINES = 4000000
def main():
    '''main'''
    directory = os.path.join(os.path.dirname(__file__),"..",'test_data')
    if not os.path.exists(directory):
        os.mkdir(directory)
    else:
        shutil.rmtree(directory)
        os.mkdir(directory)
    raw_files = dict()
    test_files = dict()
    raw_files['metag']="/global/dna/dm_archive/rqc/analyses/AUTO-137286/12074.1.235970.AGCTAAC-GGTTAGC.filter-METAGENOME.fastq.gz"
    #raw_files['metat']="/global/dna/dm_archive/rqc/analyses/AUTO-222106/12815.2.288358.TAGTGACC-GGTCACTA.filter-MTF.fastq.gz"
#    raw_files['metat']="/global/dna/dm_archive/rqc/analyses/AUTO-137356/12057.6.235819.GCTGGAT-AATCCAG.filter-MTF.fastq.gz"
    for type in raw_files:
        if not(os.path.exists(raw_files[type])):
            sys.exit("File " + raw_files[type] + " does not exist. Try:\nmodule load jamo;jamo fetch all filename " + os.path.basename(raw_files[type]))

    for type in raw_files:
        print("creating " + type)
        test_files[type]=os.path.join(os.path.dirname(__file__),"..",'test_data',os.path.basename(raw_files[type]))
        cmd = "gunzip -c " + raw_files[type] + " | head -" + str(HEAD_LINES) + " | gzip -2 >| " + test_files[type]
        out = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines() 
        cmd = "cksum " + test_files[type] + " >| " + test_files[type] + ".cksum"
        out = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines() 
    

    
if __name__ == "__main__":
    main()
