#!/usr/bin/env python

import sys, os
sys.path.insert(0,os.path.join(os.path.dirname(os.path.realpath(__file__)), "..","lib"))
import argparse
import json
import subprocess
import shutil
import re
import time
import hashlib
import report_utils as rep
import glob

'''
'''
if sys.version_info[0] <3:
    sys.stderr.write('use python3' + "\n")
    sys.exit(1)

VERSION = "1.0.0"
def main():
    ''' '''

    parser = argparse.ArgumentParser(description=main.__doc__)
    parser.add_argument("-j", "--inputjson", required=False, help="cromwell in.json for .metadata.setup_json field. default=setup.json\n")
    parser.add_argument("-o", "--outputdir", required=True, help="final input/output  dir.\n")
    parser.add_argument("--verbose", required=False, default=False,action='store_true', help="verbose mode")
    parser.add_argument('-v', '--version', action='version', version="%(prog)s (version " + VERSION + ")")
    args = parser.parse_args()

    if args.outputdir and not os.path.exists(args.outputdir):
        sys.stderr.write(args.outputdir + " does not exist \n")
        sys.exit(255)
    args.outputdir = os.path.abspath(args.outputdir)

    if not args.inputjson:
        args.inputjson = os.path.join(args.outputdir,"setup.json")
        setup_json_file = args.inputjson
        if not os.path.exists(setup_json_file):
            sys.stderr.write(setup_json_file + " not found")
            sys.exit(277)
    else:
        with open(args.inputjson) as f:json_file = json.loads(f.read())
        setup_json_file = json_file['metadata']['setup_json']
        

    sys.stdout.write( "Creating reports, " + get_timestamp() + "\n");sys.stdout.flush()
    reports_dir = os.path.join(args.outputdir,'report')
    os.mkdir(reports_dir)    
    script = os.path.join(os.path.dirname(__file__),"report_metag_bbcms_spades.py")
    cmd = script + ' -i ' + args.outputdir + ' -o ' + reports_dir + ' -j ' + setup_json_file + ' --cromwell  1> report.o 2> report.e'
    res = rep.run_process(cmd)

    sys.stdout.write( "Finalizing for submission, " + get_timestamp() + "\n");sys.stdout.flush()        
    script = os.path.join(os.path.dirname(__file__),"post_meta_assembly.py")
    report_metadata_json = os.path.join(reports_dir, "metadata.json")
    cmd = script + ' -i ' + args.outputdir + ' -o ' + reports_dir + ' -j ' + report_metadata_json + ' --cromwell 1>> report.o 2>> report.e || '
    cmd += script + ' -i ' + args.outputdir + ' -o ' + reports_dir + ' -j ' + report_metadata_json + ' --no_rqc_pdf --cromwell 1>> report.o 2>> report.e '
    res = rep.run_process(cmd)
    sys.stdout.write( "pipeline Completed successfully, " + get_timestamp() + "\n");sys.stdout.flush()        
    sys.exit()

    

def get_timestamp():
    '''return timestamp in format "%Y-%m-%d %H:%M:%S"'''
    return time.strftime("%Y-%m-%d %H:%M:%S")



if __name__ == '__main__':
    main()
