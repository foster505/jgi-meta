#!/usr/bin/env python

import os, sys
import json
import re
import glob
import argparse
import smtplib
import email
import textwrap
import getpass
import socket
from email.mime.multipart import MIMEMultipart

sys.path.insert(0, os.path.dirname(os.path.realpath(__file__)) + '/../lib')
import report_utils as rep
#import qcutils

METAGENOME_EMAIL = 'Auto-Email_MetaG_MetaT-10-17-2017.doc.txt.html'
METAGENOME_EMAIL = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..", "templates",  METAGENOME_EMAIL)
                                

FUNGAL_EMAIL = 'Auto-Email_MetaG_Fungal_MetaT-10-23-2017.doc.txt.html'
FUNGAL_EMAIL =  os.path.join(os.path.dirname(os.path.realpath(__file__)) , ".." , "templates",  FUNGAL_EMAIL)


def main():
     parser = argparse.ArgumentParser(description='Description.')
     choices = ["user", "execute"]
     parser.add_argument('-m', "--mode", required=True, dest='mode', action='store',choices=choices, help="product type to run.\n")
     parser.add_argument("-d", "--dir", required=True, help="run directory.\n")
     parser.add_argument("-n", "--no_rqc_pdf", required=False, default=False, action='store_true', help="run directory.\n")
     args = parser.parse_args()
     
     succeeded = dir_to_collab_mail(args.dir,mode=args.mode, no_rqc_pdf=args.no_rqc_pdf)
     if not succeeded:
          sys.stdout.write("collab mail failed")
          sys.exit(1)

     succeeded  = dir_to_asm_mail(args.dir,mode=args.mode)
     if not succeeded:
          sys.stdout.write("asm email failed")
          sys.exit(1)

def dir_to_collab_mail(directory,mode="",no_rqc_pdf=False):
     '''
     dir_to_collab_mail
     input = runfolder_directory
     output: mails collaborator email
     '''
     metadata_json = glob.glob(os.path.join(directory,"*.metadata.json"))
     if len(metadata_json)==1:
          metadata_json = metadata_json[0]
     elif len(metadata_json)!=1:
          assy_json = [ filename for filename in metadata_json if "assy.metadata" in filename]
          metadata_json = assy_json[0]
     with open(metadata_json,"r") as f:
          metadata = json.loads(f.read())

     pdf_file = ""
     for output in metadata['outputs']:
          if output['label'] == "rqc_graphical_report" :
               pdf_file = os.path.join(directory,output['file'])
               if not os.path.exists(pdf_file):
                    sys.exit("Can't find " + pdf_file)

     task_id = metadata['metadata']['analysis_task_id'][0]
     task = rep.web_services("pmo", "at",task_id)
     if len(task)!=1:
          sys.exit("Somthing wrong with task id " + task_id)
     su = metadata['metadata']['seq_unit_name'][0]
     dump = rep.web_services('rqc', 'su', su)
     email_template = METAGENOME_EMAIL
     if 'Fungal' in dump['library_info']['account_jgi_sci_prog']:
          email_template = FUNGAL_EMAIL

     ap_type = task['uss_rw_analysis_task']['analysis_project']['analysis_product_type_name']
     ap_name = task['uss_rw_analysis_task']['analysis_project']['analysis_project_name']
     ap_name = re.sub(r'Annotation',r'',ap_name)
     subject = ap_type + ": " + str(metadata['metadata']['sequencing_project_id'][0]) + " - " + ap_name
     pdf_file_name = "../rqc-stats.pdf"
     
     #get input
     dump = rep.web_services('jamo', 'json', {"file_name":su})[0]
     proposal_id = dump['metadata']['proposal']['id']
     proposal_title = dump['metadata']['proposal']['title']
     pi_first_name = dump['metadata']['proposal']['pi']['first_name'].capitalize()
     pi_last_name = dump['metadata']['proposal']['pi']['last_name'].capitalize()
     pi_email = dump['metadata']['proposal']['pi']['email_address']
     pm_last_name = dump['metadata']['proposal']['default_project_manager']['last_name'].capitalize()
     pm_first_name = dump['metadata']['proposal']['default_project_manager']['first_name'].capitalize()
     pm_email = dump['metadata']['proposal']['default_project_manager']['email_address']
     #proposal_id, proposal_title, pi_name, pi_email = qcutils.get_proposal_info(metadata['metadata']['sequencing_project_id'][0], verbose=False)
     #pi_last_name, pi_first_name = [i.capitalize() for i in pi_name.split(", ")]
     #pm_name, pm_email  = qcutils.get_project_manager(metadata['metadata']['sequencing_project_id'][0], verbose=False)
     #pm_last_name, pm_first_name = pm_name.split(", ")
     #tmpfile = "tmpmail" + str(calendar.timegm(time.gmtime())) + ".txt.html"
    
     #ap = qcutils.get_analysis_project(metadata['metadata']['analysis_project_id'][0], verbose=False)
     ap = rep.web_services('pmo', 'ap', metadata['metadata']['analysis_project_id'][0])['uss_analysis_project']
     fd =  ap['final_deliv_project_id']
     string_list = 'pi_last_name pi_first_name pi_email pm_last_name pm_first_name pm_email'
     string_list = tuple(string_list.split())
     subst = {'{{' + value + '}}': value for value in string_list}
     subst["{{JGI_genome_portal_page.}}"]='<a href="https://genome.jgi.doe.gov/lookup?keyName=jgiProjectId&keyValue=' + str(fd)  + '&app=Info">JGI genome portal page.</a>'
     subst["{{IMG/M}}"]='<a href="https://img.jgi.doe.gov/">IMG/M</a>'
     subst["{{SRA}}"]='<a href="https://www.ncbi.nlm.nih.gov/sra">SRA</a>'
     subst["{{Standards_in_Genomic_Sciences}}"]='<a href="https://standardsingenomics.biomedcentral.com/submission-guidelines/preparing-your-manuscript/metagenome-report">Standards in Genomic Sciences</a>'
     subst["{{Microbiome}}"]='<a href="https://microbiomejournal.biomedcentral.com/submission-guidelines/preparing-your-manuscript/microbiome-announcement">Microbiome</a>'
     subst["{{Scientific_Data}}"]='<a href="https://www.nature.com/sdata/publish/for-authors">Scientific Data</a>'
     subst["{{DOE_Auspice}}"]='<a href="https://jgi.doe.gov/user-program-info/pmo-overview/policies">DOE Auspice Statement</a>'


     with open(email_template, "r") as f:
          email = f.read()

     email_body = ""
     email_body += "<html>\n<body>\n"
     for line in email.split("\n"):
          if re.match(r'^\n',line):
               line = re.sub(r'^\n','<br>',line)
          elif len(line) > 50:
               line = "\n<br>".join(textwrap.wrap(line, width=80)) + "<br>"
          else:
               line = line + "<br>"
          for key in subst.keys():
               if "href" in subst[key]:
                    line = re.sub(key, subst[key], line)
               else:
                    line = re.sub(key, eval(subst[key]), line)
          email_body += line
     email_body += "\n</body>\n</html>\n"

     sender = getpass.getuser() + '@lbl.gov'
     debug=False
     recipient = ""
     if mode == "user":
          recipient = getpass.getuser() + '@lbl.gov'
     elif mode == "execute":
          recipient = pi_email + ',' + pm_email + ',' + 'bfoster@lbl.gov'
          recipient += ',accopeland@lbl.gov,aclum@lbl.gov,eaeloefadrosh@lbl.gov'

     reply_to = "bfoster@lbl.gov,eaeloefadrosh@lbl.gov," + pm_email
     if no_rqc_pdf==True or pdf_file=="":
          success = rep.send_mail(subject=subject, body=email_body, sender=sender, reply_to=reply_to.split(","), recipients=recipient.split(",")) 
     else:
          if not (os.path.exists(pdf_file)):
               sys.exit(pdf_file + " does not exist")
          success = rep.send_mail(subject=subject, body=email_body, sender=sender, reply_to=reply_to.split(","), recipients=recipient.split(","), attachments=[pdf_file])
     return success


def dir_to_asm_mail(directory,mode=""):
     '''
     dir_to_asm_mail
     input = runfolder_directory
     output: mails asm_release <l
     '''
     metadata_json = glob.glob(os.path.join(directory,"*.metadata.json"))
     if len(metadata_json)==1:
          metadata_json = metadata_json[0]
     elif len(metadata_json)!=1:
          assy_json = [ filename for filename in metadata_json if "assy.metadata" in filename]
          metadata_json = assy_json[0]
     with open(metadata_json,"r") as f:
          metadata = json.loads(f.read())
     
     text_report = ""
     pdf_report = ""
     pdf_file = ""
     for output in metadata['outputs']:
          if output['label'] == "text_report":
               text_file = os.path.join(directory,output['file'])
               if not os.path.exists(text_file):
                    sys.exit("Can't find " + text_file)

          if output['label'] == "readme":
               text_file = os.path.join(directory,output['file'])
               if not os.path.exists(text_file):
                    sys.exit("Can't find " + text_file)

          if output['label'] == "graphical_report":
               pdf_file = os.path.join(directory,output['file'])
               if not os.path.exists(pdf_file):
                    sys.exit("Can't find " + pdf_file)

     task_id = metadata['metadata']['analysis_task_id'][0]
     task = rep.web_services("pmo", "at",task_id)
     if len(task)!=1:
          sys.exit("Somthing wrong with task id " + task_id)

     ap_type = task['uss_rw_analysis_task']['analysis_project']['analysis_product_type_name']
     ap_name = task['uss_rw_analysis_task']['analysis_project']['analysis_project_name']
     ap_name = re.sub(r'Annotation',r'',ap_name)
     subject = '[asm_release] ' + ap_type + ": " + str(metadata['metadata']['sequencing_project_id'][0]) + " - " + ap_name
     sender = 'bfoster@lbl.gov'
     reply_to = ['bfoster@lbl.gov']
     recipients = ['bfoster@lbl.gov']

     with open(text_file, "r") as f:
          body = f.read()
     body = text2html(text=body)

     if mode == "user":
          recipients =[ getpass.getuser() + '@lbl.gov']
     elif mode == "execute":
          recipients =  ['jgi-asm_release@lists.lbl.gov']

     if pdf_file:
          success = rep.send_mail(subject=subject, body=body, sender=sender, reply_to=reply_to, recipients=recipients, attachments=[pdf_file])
     else:
          success = rep.send_mail(subject=subject, body=body, sender=sender, reply_to=reply_to, recipients=recipients)
     return success


     
def text2html(**kwargs):
     '''
        text2html 
        keyword arguments:
        images: list of valid image paths
        text: text for body of email
     '''

    #validate images
     image_text = ""
     if 'images' in kwargs.keys():
          images = kwargs['images']
     else:
          images = []

     for image in images:
          if not os.path.exists(image):
               sys.exit("Problem with Image:" + image + "\n")
          else:
               image_text = "<img src=\"" + image + "\"/>\n"
     html = ""
     html += "<html>\n"
     html += "<head>\n"
     html += "</head>\n"
     html += '<body bgcolor="#ffffff" text="#000000" link="#0000ff" vlink="#ff0000" alink="#ffff00">' + "\n"
     html += "<pre>\n"
     html += kwargs['text']
     html += "</pre>\n"
     html += image_text
     html += "</body>"
     html += "</html>"
     return  html


if __name__ == "__main__":
    main()

