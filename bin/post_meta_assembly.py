#!/usr/bin/env python

import argparse
import sys
import os
import json
import time
import glob

#genepool and denovo
RQC_JAT_CHECK = "/global/dna/projectdirs/PI/rqc/prod/jgi-rqc-pipeline/report/rqc_report.py"

sys.path.insert(0,os.path.join(os.path.dirname(os.path.realpath(__file__)), "../lib"))
import report_utils as rep
VERSION = "1.0.0"

'''
jgi_mt_stats.py 
given a run directory and metadata.json:
 
add rqc-stats.pdf rqc-stats.txt and,
sigs tables:
    Table_3_library_information.txt
    Table_4_sequence_processing.txt
    Table_5_metagenome_statistics.txt

'''

def main():
    '''main'''
    metag_products = ['metag','viralmetag']
    metat_products = ['metat', 'eukmetat']
    choices = metag_products + metat_products
    parser = argparse.ArgumentParser(description='Description.')
    parser.add_argument('-v', '--version', action='version', version="%(prog)s (version " + VERSION + ")")
    parser.add_argument("-i", "--inputdir", required=True, help="input directory.\n")
    parser.add_argument("-o", "--outputdir", required=True, help="output directory.\n")    
    parser.add_argument("-j", "--json", required=True, help="json file with project metadata .\n")
    
    parser.add_argument('-p', "--product", required=False, dest='product', action='store',choices=choices, help="product type to run not required if input json had info.\n")
    #parser.add_argument("-l", "--legacy_templates", required=False, default=False, action='store_true', help="to specify creating 2 AT metadata.jsons for metat.\n")
    parser.add_argument("-n", "--no_rqc_pdf", required=False, default=False, action='store_true', help="to skip rqc_pdf.\n")
    parser.add_argument("-d", "--debug", required=False, default=False, action='store_true', help="debug.\n")
    args = parser.parse_args()

    if not(os.path.exists(args.outputdir)):
        os.mkdir(args.outputdir)
        
        
    if not os.path.exists(args.json):
        sys.exit("Can't find " + args.json)
    with open(args.json, "r") as f:
        meta = json.loads(f.read())
    args.inputdir = os.path.realpath(args.inputdir)
            
    if os.path.exists(args.inputdir) == 0:
        sys.exit("No inputdir specified")
    if not args.inputdir.startswith('/'):
        sys.exit("Use full path for input directory")

    product_set_id =0
    product_set_id_type = ""
    if 'pipeline_metadata' in meta['metadata']:
        product_set_id = meta['metadata']['pipeline_metadata']['execution_metadata']['templates']['id']
        
    elif args.product:
        if args.product in['metag']:
            product_set_id_type = 'metag'
            product_set_id = 2
        elif args.product in['metat','eukmetat']:
            product_set_id_type = 'metat'
            product_set_id = 3
        elif args.product in['viralmetag']:
            product_set_id_type = 'metag'
            product_set_id = 4
    else:
        sys.exit("can't determine which product to run. Try -p option.")
    
    if product_set_id in[2,4]:
        product_set_id_type='metag'
    elif product_set_id==3:
        product_set_id_type='metat'
    else:
        sys.exit("can't determine which product to run. Try -p option.")
        
    #create rqcpdf, kvp (with_sanity), sigs and update metadata.json
    #gather paths
    paths = dict()
    if product_set_id ==2:#regular metagenome pipeline
        paths['readstats']=rep.get_glob(os.path.join(args.inputdir,"readstats_bbcms","readlen.txt"))
        paths['covstats']=rep.get_glob(os.path.join(args.inputdir,"read_mapping_pairs", "covstats.txt"))
        paths['reproduce']=rep.get_glob(os.path.join(args.inputdir,"reproduce", "reproduce.bash"))
    elif product_set_id ==4:#low input viral
        paths['readstats']=rep.get_glob(os.path.join(args.inputdir,"readstats_dedupe","readlen.txt"))
        paths['covstats']=rep.get_glob(os.path.join(args.inputdir,"read_mapping_pairs", "covstats.txt"))
        paths['reproduce']=rep.get_glob(os.path.join(args.inputdir,"reproduce", "reproduce.bash"))
    elif product_set_id ==3:#low input viral
        paths['readstats']=rep.get_glob(os.path.join(args.inputdir,"readstats_raw","readlen.txt"))
        paths['covstats']=rep.get_glob(os.path.join(args.inputdir,"read_mapping_pairs", "covstats.txt"))
        paths['reproduce']=rep.get_glob(os.path.join(args.inputdir,"reproduce", "reproduce.bash"))
    else:
        exit("product_set_id:"  + str(product_set_id) + " does not exist")

    kvp=dict()
    kvp["sanity_check.release_spid_in_jamo"] = ",".join([rep.jamo_release_count(spid,type_id='spid') for spid in meta['metadata']['sequencing_project_id' ]])
    kvp["sanity_check.release_ap_in_jamo"] = ",".join([rep.jamo_release_count(atid,type_id='atid') for atid in meta['metadata']['analysis_task_id' ]])
    #kvp["sanity_check.release_ap_to_taxonoid"] = rep.its_ap_to_taxonoid(atid)

    kvp["stats.date"] = time.strftime("%Y%m%d")
    for output in meta["outputs"]:
        if "metadata" in output:
            for key in output["metadata"]:
                kvp["outputs." + output["label"] + "." + key] = output["metadata"][key]
                
    kvp = rep.get_sigs_table(meta, kvp, paths['readstats'], paths['covstats'])
    sigs = rep.create_sig_subset(kvp)
    
    for table_id in list(sigs.keys()):
        filename = os.path.join(args.outputdir, sigs[table_id]['filename'])
        with open(filename, "w") as f:
            for line in sigs[table_id]['data']:
                f.write(line + "\n")
        relative_filename = os.path.join(os.path.basename(os.path.dirname(filename)),os.path.basename(filename))
        meta['outputs'].append({"label":"sigs_" + table_id + "_report","file":relative_filename,"metadata":{"file_format":"text"}})


    kvp.update(rep.get_logical_amounts(meta['metadata']['seq_info']))#adds combined info and cumulative logical sequence
    #with open(kvpfile,"w") as f:f.write(json.dumps(kvp,indent=3)) below is the safe way
    rqc_stats =  os.path.join(args.inputdir,"rqc-stats.txt")  
    with open(rqc_stats, "w") as f:
        for key in sorted(kvp.keys()):
            string_ = str()
            try:
                string_ = str(kvp[key])
            except:
                string_ = kvp[key].encode('utf-8')
            f.write(key + "=" + string_ + "\n")
    
    submit = rqc_stats
    file_of_files =""
    if product_set_id_type in ['metat', 'metag']:
        file_of_files = create_metagenome_file_of_files(os.path.join(args.inputdir,"report"),os.path.join(args.inputdir,"rqc-paths.txt"))
        submit = submit + "," + file_of_files


    if not args.no_rqc_pdf and len(meta['inputs'])==1:
        #label: rqc_graphical_report
        pdf_file = os.path.join(args.outputdir,"rqc-stats.pdf")
        submit_files = ""
        if file_of_files:
            submit_files = ",".join([rqc_stats,file_of_files])
        else:
            submit_files = rqc_stats
        rep.generate_rqc_report(sequnits=meta['metadata']['seq_unit_name'], run_type=product_set_id_type, outputfile=pdf_file, rqcstats=submit_files,debug=True)
        relative_filename = os.path.join(os.path.basename(os.path.dirname(pdf_file)),os.path.basename(pdf_file))
        meta['outputs'].append({"label":"rqc_graphical_report","file":relative_filename,"metadata":{"file_format":"pdf"}})
    
    if product_set_id_type in ['metat']:
        #label: shell_script
        filename = os.path.realpath(paths['reproduce'])
        relative_filename =   os.path.join(os.path.basename(os.path.dirname(filename)),os.path.basename(filename))
        meta['outputs'].append({"label":"shell_script","file":relative_filename, "metadata":{"file_format":"sh"}})

    
    ############create SIG table text file
    sigs= rep.hasher()
    sigs = rep.create_sig_subset(kvp)
    sigs['3']['filename'] = 'Table_3_library_information.txt'
    sigs['4']['filename'] = 'Table_4_sequence_processing.txt'
    sigs['5']['filename'] = 'Table_5_metagenome_statistics.txt'

    meta['metadata'] = {k:meta['metadata'][k] for k in meta['metadata'].keys() if k not in['pipeline_metadata', 'proposal', 'seq_info']}
    
    output_metadata_json = os.path.join(args.inputdir,str(meta['metadata']['analysis_task_id'][0]) + '.metadata.json')
    with open(output_metadata_json, "w") as f:f.write(json.dumps(meta,indent = 3))

    return

def create_metagenome_file_of_files(report_dir, outpath):
#    report_avg_fold_vs_len.bmp=/global/projectb/sandbox/gaag/bfoster/metaG/debug/genepool_spades_final/12/1125622/report/report_avg_fold_vs_len.bmp
#    assembledTaxMap=/global/projectb/sandbox/gaag/bfoster/metaG/debug/genepool_spades_final/12/1125622/report/../taxMap/out.sam.gz.tax.sorted
#    report_gc.bmp=/global/projectb/sandbox/gaag/bfoster/metaG/debug/genepool_spades_final/12/1125622/report/report_gc.bmp
#    1125471.README.txt=/global/projectb/sandbox/gaag/bfoster/metaG/debug/genepool_spades_final/12/1125622/1125471.README.txt
#    report_gc_vs_avg_fold.bmp=/global/projectb/sandbox/gaag/bfoster/metaG/debug/genepool_spades_final/12/1125622/report/report_gc_vs_avg_fold.bmp
#    1125471.README.pdf=/global/projectb/sandbox/gaag/bfoster/metaG/debug/genepool_spades_final/12/1125622/1125471.README.pdf
#    report.release.html=/global/projectb/sandbox/gaag/bfoster/metaG/debug/genepool_spades_final/12/1125622/report/report.release.html
    images = glob.glob(os.path.join(report_dir,'*.bmp'))
    with open(outpath, "w") as f:
        for file_name in images:
            f.write(os.path.basename(file_name) + "=" + file_name + "\n")
    return outpath

                          
if __name__ == "__main__":
    main()



